<?php

namespace Tests\Behat\Context;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Validator\Constraints\Date;
use Webmozart\Assert\Assert;
use InvalidArgumentException;
use DateTime;

/**
 * Class ApiContext
 * @package Tests\Behat\Context
 */
class ApiContext extends FeatureContext
{
    /**
     * Success status code
     */
    const SUCCESS_STATUS_CODE = 200;

    /**
     * Note resource uri address
     */
    const NOTE_RESOURCE_URI = '/notes';

    /**
     * Base api url address
     */
    const BASE_URL = 'http://api.note.dev:8080';

    /**
     * Http client
     *
     * @var Client
     */
    private $httpClient;

    /**
     * Response
     *
     * @var ResponseInterface
     */
    private $response;

    /**
     * ApiContext constructor.
     */
    public function __construct()
    {
        $this->httpClient = new Client([
            'base_uri' => self::BASE_URL
        ]);
    }

    /**
     * @Given I send request to get note with id :noteId
     *
     * @param int $noteId
     */
    public function sendRequestToGetSingleNote($noteId)
    {
        $this->response = $this->httpClient->get(self::NOTE_RESOURCE_URI . '/' . $noteId);
    }

    /**
     * @Then The response content should be JSON
     */
    public function responseIsInValidJsonFormat()
    {
        Assert::true($this->isValidJson((string)$this->response->getBody()));
    }

    /**
     * @Then The response status code should be :statusCode
     *
     * @param int $statusCode
     */
    public function responseStatusCodeShouldBe($statusCode)
    {
        Assert::eq($statusCode, $this->response->getStatusCode());
    }

    /**
     * @Then The response content contains valid single note data
     */
    public function responseShouldContainsValidNoteData()
    {
        $this->isNoteValid(json_decode((string)$this->response->getBody(), true));
    }

    /**
     * @Then The response content contains valid notes collection data
     */
    public function responseShouldContainsValidNotesCollectionData()
    {
        $arrayData = json_decode((string)$this->response->getBody(), true);
        $this->isMetadataValid($arrayData['metadata']);

        Assert::lessThanEq($arrayData['metadata']['limit'], count($arrayData['results']));
        foreach ($arrayData['results'] as $note) {
            $this->isNoteValid($note);
        }
    }

    /**
     * @Given I send request to get notes collection with :page :limit :remembered :orderBy
     *
     * @param int $page
     * @param $limit
     * @param $remembered
     * @param $orderBy
     */
    public function sendRequestToGetNotesCollection($page = 1, $limit, $remembered, $orderBy)
    {
        $this->response = $this->httpClient->get(self::NOTE_RESOURCE_URI, [
            'query' => [
                'page' => $page,
                'limit' => $limit,
                'orderBy' => $orderBy,
                'remembered' => $remembered
            ]
        ]);
    }

    /**
     * @Then I sent request to create new note with :title :content :remembered
     *
     * @param string $title
     * @param string $content
     * @param bool $remembered
     */
    public function sendRequestToCreateNote($title, $content, $remembered)
    {
        $this->response = $this->httpClient->post(self::NOTE_RESOURCE_URI, [
            'json' => [
                'title' => $title,
                'content' => $content,
                'remembered' => filter_var($remembered, FILTER_VALIDATE_BOOLEAN)
            ]
        ]);
    }

    /**
     * @Then  I sent request to update note :noteId with :title :content :remembered
     *
     * @param int $noteId
     * @param string $title
     * @param string $content
     * @param bool $remembered
     */
    public function sendRequestToUpdateNote($noteId, $title, $content, $remembered)
    {
        $this->response = $this->httpClient->put(self::NOTE_RESOURCE_URI . '/' . $noteId, [
            'json' => [
                'title' => $title,
                'content' => $content,
                'remembered' => filter_var($remembered, FILTER_VALIDATE_BOOLEAN)
            ]
        ]);
    }

    /**
     * @Then Created note should contains :title :content :remembered
     *
     * @param string $title
     * @param string $content
     * @param bool $remembered
     */
    public function createdNoteShouldContainsCorrectData($title, $content, $remembered)
    {
        $remembered = filter_var($remembered, FILTER_VALIDATE_BOOLEAN);
        $arrayData = json_decode((string)$this->response->getBody(), true);

        Assert::same($title, $arrayData['title']);
        Assert::same($content, $arrayData['content']);
        Assert::same($remembered, $arrayData['remembered']);
    }

    /**
     * Check if given string is valid json format
     *
     * @param string $string
     * @return bool
     */
    private function isValidJson($string)
    {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * Check if given string is valid datetime
     *
     * @param string $stringDate
     * @param string $format
     * @return bool
     */
    private function validateDate($stringDate, $format = DateTime::ISO8601)
    {
        $date = DateTime::createFromFormat($format, $stringDate);
        return $date && $date->format($format) == $stringDate;
    }

    /**
     * Check if given note array date is valid
     *
     * @param array $arrayNote
     *
     * @throws InvalidArgumentException
     */
    private function isNoteValid(array $arrayNote)
    {
        Assert::integer($arrayNote['id']);
        Assert::notEmpty($arrayNote['id']);
        Assert::string($arrayNote['title']);
        Assert::notEmpty($arrayNote['title']);
        Assert::string($arrayNote['content']);
        Assert::notEmpty($arrayNote['content']);
        Assert::boolean($arrayNote['remembered']);
        Assert::notEmpty($arrayNote['created_at']);
        Assert::notEmpty($arrayNote['updated_at']);
        Assert::true($this->validateDate($arrayNote['created_at']));
        Assert::true($this->validateDate($arrayNote['updated_at']));
    }

    /**
     * Check if given notes collection metadata is valid
     *
     * @param array $arrayData
     *
     * @throws InvalidArgumentException
     */
    private function isMetadataValid(array $arrayData)
    {
        Assert::notEmpty($arrayData['page']);
        Assert::integer($arrayData['page']);
        Assert::notEmpty($arrayData['limit']);
        Assert::integer($arrayData['limit']);
        Assert::notEmpty($arrayData['totalItems']);
        Assert::integer($arrayData['totalItems']);
        Assert::boolean($arrayData['remembered']);
        Assert::notEmpty($arrayData['totalPages']);
        Assert::integer($arrayData['totalPages']);
    }
}