<?php

namespace Tests\Behat\Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;


/**
 * Class FeatureContext
 * @package Tests\Behat\Context
 */
class FeatureContext implements Context, SnippetAcceptingContext
{

    /**
     * FeatureContext constructor.
     */
    public function __construct()
    {

    }
}
