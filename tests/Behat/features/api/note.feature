Feature: Test note resource

  Scenario: User can get note
    Given I send request to get note with id "1"
    And The response status code should be "200"
    And The response content should be JSON
    Then The response content contains valid single note data

  Scenario: User can get note collection
    Given I send request to get notes collection with "1" "25" "false" "createdAt"
    And The response status code should be "200"
    And The response content should be JSON
    Then The response content contains valid notes collection data

  Scenario: User can create new note
    Given I sent request to create new note with "Random title" "Kick ass content" "false"
    And The response status code should be "200"
    And The response content should be JSON
    And The response content contains valid single note data
    Then Created note should contains "Random title" "Kick ass content" "false"

  Scenario: User can update note
    Given I sent request to update note "1" with "Random title" "Kick ass content" "false"
    And The response status code should be "200"
    And The response content should be JSON
    And The response content contains valid single note data
    Then Created note should contains "Random title" "Kick ass content" "false"