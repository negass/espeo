<?php

namespace Tests\Unit\AppBundle\Service\Note;

use AppBundle\Service\Note\NoteService;
use AppBundle\Repository\Note\NoteRepositoryInterface;
use AppBundle\Entity\Note\NoteInterface;
use AppBundle\Repository\Note\Exception\NoteRepositoryException;
use Mockery;

/**
 * Class NoteServiceTest
 *
 * @package Tests\Unit\AppBundle\Service\Note
 *
 * @group service
 * @group note_service
 * @coversDefaultClass AppBundle\Service\Note\NoteService
 */
class NoteServiceTest extends \PHPUnit_Framework_TestCase
{
    use NoteServiceDataProvider;

    /**
     * Get note object mock
     *
     * @return NoteInterface
     */
    private function getNoteObjectMock()
    {
        return Mockery::mock(NoteInterface::class);
    }

    /**
     * Get repository mock
     *
     * @return NoteRepositoryInterface
     */
    private function getNoteRepositoryMock()
    {
        $mock = Mockery::mock(NoteRepositoryInterface::class);
        $mock->shouldReceive('getById')->andReturn($this->getNoteObjectMock());
        $mock->shouldReceive('getCollection')->andReturn([]);
        $mock->shouldReceive('count')->andReturn(5);
        $mock->shouldReceive('save');

        return $mock;
    }

    /**
     * Get repository mock
     *
     * @return NoteRepositoryInterface
     */
    private function getErrorNoteRepositoryMock()
    {
        $mock = Mockery::mock(NoteRepositoryInterface::class);
        $mock->shouldReceive('getById')->andThrow(NoteRepositoryException::class);
        $mock->shouldReceive('count')->andThrow(NoteRepositoryException::class);
        $mock->shouldReceive('save')->andThrow(NoteRepositoryException::class);
        $mock->shouldReceive('getCollection')->andThrow(NoteRepositoryException::class);

        return $mock;
    }

    /**
     * Test get by id
     *
     * @covers ::getById
     * @dataProvider correctDataForGetById
     */
    public function testGetById($noteId)
    {
        $noteService = new NoteService($this->getNoteRepositoryMock());
        $noteObject = $noteService->getById($noteId);

        $this->assertInstanceOf(NoteInterface::class, $noteObject);
    }

    /**
     * Test get by id for negative scenario
     *
     * @covers ::getById
     * @dataProvider correctDataForGetById
     * @expectedException \AppBundle\Service\Note\Exception\NoteServiceException
     */
    public function testGetByIdForNegativeScenario($noteId)
    {
        $noteService = new NoteService($this->getErrorNoteRepositoryMock());
        $noteService->getById($noteId);
    }

    /**
     * Test save
     *
     * @covers ::save
     */
    public function testSave()
    {
        $noteService = new NoteService($this->getNoteRepositoryMock());
        $noteObject = $this->getNoteObjectMock();
        $savedObject = $noteService->save($noteObject);

        $this->assertInstanceOf(NoteInterface::class, $savedObject);
    }

    /**
     * Test save for negative scenario
     *
     * @covers ::save
     * @expectedException \AppBundle\Service\Note\Exception\NoteServiceException
     */
    public function testSaveForNegativeScenario()
    {
        $noteService = new NoteService($this->getErrorNoteRepositoryMock());
        $noteObject = $this->getNoteObjectMock();
        $noteService->save($noteObject);
    }

    /**
     * Test count
     *
     * @param bool $remembered
     *
     * @covers ::count
     * @dataProvider correctDataForCount
     */
    public function testCount($remembered)
    {
        $noteService = new NoteService($this->getNoteRepositoryMock());
        $countValue = $noteService->count($remembered);

        $this->assertInternalType('integer', $countValue);
    }

    /**
     * Test count for negative scenario
     *
     * @covers ::count
     * @dataProvider correctDataForCount
     * @expectedException \AppBundle\Service\Note\Exception\NoteServiceException
     */
    public function testCountForNegativeScenario($remembered)
    {
        $noteService = new NoteService($this->getErrorNoteRepositoryMock());
        $noteService->count($remembered);
    }

    /**
     * Test get collection method
     *
     * @param bool $remembered
     * @param string $orderBy
     * @param int $limit
     * @param int $offset
     *
     * @covers ::getCollection
     * @dataProvider correctDataForGetCollection
     */
    public function testGetCollection($remembered, $orderBy, $limit, $offset)
    {
        $noteService = new NoteService($this->getNoteRepositoryMock());
        $list = $noteService->getCollection($remembered, $orderBy, $limit, $offset);

        $this->assertInternalType('array', $list);
    }

    /**
     * Test get collection for negative scenario
     *
     * @covers ::getCollection
     * @dataProvider correctDataForGetCollection
     * @expectedException \AppBundle\Service\Note\Exception\NoteServiceException
     */
    public function testGetCollectionForNegativeScenario($remembered, $orderBy, $limit, $offset)
    {
        $noteService = new NoteService($this->getErrorNoteRepositoryMock());
        $noteService->getCollection($remembered, $orderBy, $limit, $offset);
    }
}
