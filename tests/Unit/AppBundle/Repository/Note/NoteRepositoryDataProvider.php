<?php

namespace Tests\Unit\AppBundle\Repository\Note;

/**
 * Class NoteRepositoryDataProvider
 * @package Tests\Unit\AppBundle\Repository\Note
 */
trait NoteRepositoryDataProvider
{
    /**
     * Correct data provider for get by id method
     */
    public function correctDataForGetById()
    {
        return [
            ['1'],
            [223],
            [99]
        ];
    }

    /**
     * Correct data provider for count method
     */
    public function correctDataForCount()
    {
        return [
            [false],
            [true]
        ];
    }

    /**
     * Correct data provider for get collection method
     */
    public function correctDataForGetCollection()
    {
        return [
            [false, 'id', 10, 50],
            [true, 'orderBy', 0, 0],
            [true, 'orderBy', 10, 50],
            ['false', 'orderBy', 10, 50],
        ];
    }
    
}