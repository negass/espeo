<?php

namespace Tests\Unit\AppBundle\Repository\Note;

use AppBundle\Entity\Note\NoteInterface;
use Mockery;
use AppBundle\Repository\Note\NoteRepository;
use AppBundle\Repository\Note\NoteCachedRepository;
use Tedivm\StashBundle\Service\CacheService;

/**
 * Class NoteCachedRepositoryTest
 *
 * @group repository
 * @group note_repository_cached
 * @coversDefaultClass AppBundle\Repository\Note
 *
 * @package Tests\Unit\AppBundle\Repository\Note
 */
class NoteCachedRepositoryTest extends \PHPUnit_Framework_TestCase
{
    use NoteRepositoryDataProvider;

    /**
     * Get note entity mock
     *
     * @return NoteInterface
     */
    private function getNoteEntityMock()
    {
        $mock = Mockery::mock(NoteInterface::class);
        $mock->shouldReceive('getId')->andReturn(1);

        return $mock;
    }

    /**
     * Get class metadata mock
     *
     * @return CacheService
     */
    private function getCacheService()
    {
        return new CacheService('stash');
    }

    /**
     * Test get by id method
     *
     * @covers ::getById
     * @dataProvider correctDataForGetById
     */
    public function testGetById($id)
    {
        $nonCachedRepository = Mockery::mock(NoteRepository::class);
        $nonCachedRepository->shouldReceive('getById')->once()->with($id)->andReturn($this->getNoteEntityMock());
        $cachedRepository = new NoteCachedRepository($nonCachedRepository, $this->getCacheService());
        $noteFromNonCachedRepository = $cachedRepository->getById($id);
        $noteFromCachedRepository = $cachedRepository->getById($id);
        $this->assertInstanceOf(NoteInterface::class, $noteFromNonCachedRepository);
        $this->assertInstanceOf(NoteInterface::class, $noteFromCachedRepository);
        $this->assertSame($noteFromCachedRepository, $noteFromNonCachedRepository);
    }

    /**
     * Test count method
     *
     * @covers ::count
     * @dataProvider correctDataForCount
     */
    public function testCount($remembered)
    {
        $nonCachedRepository = Mockery::mock(NoteRepository::class);
        $nonCachedRepository->shouldReceive('count')->once()->with($remembered)->andReturn(5);
        $cachedRepository = new NoteCachedRepository($nonCachedRepository, $this->getCacheService());
        $noteFromNonCachedRepository = $cachedRepository->count($remembered);
        $noteFromCachedRepository = $cachedRepository->count($remembered);
        $this->assertSame($noteFromCachedRepository, $noteFromNonCachedRepository);
    }

    /**
     * Test save method
     *
     * @covers ::save
     */
    public function testSave()
    {
        $nonCachedRepository = Mockery::mock(NoteRepository::class);
        $nonCachedRepository->shouldReceive('save')->once()->andReturn($this->getNoteEntityMock());
        $cachedRepository = new NoteCachedRepository($nonCachedRepository, $this->getCacheService());
        $cachedRepository->save($this->getNoteEntityMock());

        $this->assertInstanceOf(NoteInterface::class, $this->getNoteEntityMock());
    }

    /**
     * Test get collection method
     *
     * @covers ::getCollection
     * @dataProvider correctDataForGetCollection
     *
     * @param bool $remembered
     * @param string $orderBy
     * @param int $limit
     * @param int $offset
     */
    public function testGetCollection($remembered, $orderBy, $limit, $offset)
    {
        $nonCachedRepository = Mockery::mock(NoteRepository::class);
        $nonCachedRepository->shouldReceive('getCollection')->once()->withArgs([
            $remembered,
            $orderBy,
            $limit,
            $offset
        ])->andReturn([]);
        $cachedRepository = new NoteCachedRepository($nonCachedRepository, $this->getCacheService());
        $cachedRepository->getCollection($remembered, $orderBy, $limit, $offset);
    }
}
