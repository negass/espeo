<?php

namespace Tests\Unit\AppBundle\Repository\Note;

use AppBundle\Entity\Note\NoteInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Mockery;
use AppBundle\Repository\Note\NoteRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * Class TestNoteRepository
 *
 * @group repository
 * @group note_repository
 * @coversDefaultClass AppBundle\Repository\Note
 *
 * @package Tests\Unit\AppBundle\Repository\Note
 */
class NoteRepositoryTest extends \PHPUnit_Framework_TestCase
{
    use NoteRepositoryDataProvider;

    /**
     * Get note entity mock
     *
     * @return NoteInterface
     */
    private function getNoteEntityMock()
    {
        return Mockery::mock(NoteInterface::class);
    }

    /**
     * Get class metadata mock
     *
     * @return ClassMetadata
     */
    private function getMetadata()
    {
        return Mockery::mock(ClassMetadata::class);
    }

    /**
     * Test get by id method
     *
     * @covers ::getById
     * @dataProvider correctDataForGetById
     */
    public function testGetById($id)
    {
        $noteRepositoryMock = Mockery::mock(NoteRepository::class);
        $noteRepositoryMock->shouldReceive('find')->andReturn($this->getNoteEntityMock());
        $noteRepositoryMock->makePartial();
        $note = $noteRepositoryMock->getById($id);

        $this->assertInstanceOf(NoteInterface::class, $note);
    }

    /**
     * Test get by id method for negative scenario
     *
     * @covers ::getById
     * @dataProvider correctDataForGetById
     * @expectedException \AppBundle\Repository\Note\Exception\NoteRepositoryException
     */
    public function testGetByIdForNegativeScenario($id)
    {
        $noteRepositoryMock = Mockery::mock(NoteRepository::class);
        $noteRepositoryMock->shouldReceive('find')->andReturn(null);
        $noteRepositoryMock->makePartial();
        $noteRepositoryMock->getById($id);
    }

    /**
     * Test count method
     *
     * @covers ::count
     * @dataProvider correctDataForCount
     */
    public function testCount($remembered)
    {
        $remembered = (int)$remembered;

        $queryBuilderMock = Mockery::mock(QueryBuilder::class);
        $queryBuilderMock->shouldReceive('select')->andReturnSelf();
        $queryBuilderMock->shouldReceive('where')->with("n.remembered = " . (int)$remembered)->andReturnSelf();
        $queryBuilderMock->shouldReceive('getQuery')->andReturnSelf();
        $queryBuilderMock->shouldReceive('getSingleScalarResult')->andReturn(5);

        $noteRepositoryMock = Mockery::mock(NoteRepository::class);
        $noteRepositoryMock->shouldReceive('createQueryBuilder')->andReturn($queryBuilderMock);
        $noteRepositoryMock->makePartial();

        $count = $noteRepositoryMock->count($remembered);

        $this->assertInternalType('integer', $count);
    }

    /**
     * Test count for negative scenario
     *
     * @dataProvider correctDataForCount
     * @covers ::count
     * @expectedException \AppBundle\Repository\Note\Exception\NoteRepositoryException
     */
    public function testCountForNegativeScenario($remembered)
    {
        $remembered = (int)$remembered;

        $queryBuilderMock = Mockery::mock(QueryBuilder::class);
        $queryBuilderMock->shouldReceive('select')->andReturnSelf();
        $queryBuilderMock->shouldReceive('where')->with("n.remembered = " . (int)$remembered)->andReturnSelf();
        $queryBuilderMock->shouldReceive('getQuery')->andReturnSelf();
        $queryBuilderMock->shouldReceive('getSingleScalarResult')->andReturn(5);

        $noteRepositoryMock = Mockery::mock(NoteRepository::class);
        $noteRepositoryMock->shouldReceive('createQueryBuilder')->andThrow(NoResultException::class);
        $noteRepositoryMock->shouldReceive('createQueryBuilder')->andThrow(NonUniqueResultException::class);
        $noteRepositoryMock->makePartial();

        $noteRepositoryMock->count($remembered);
    }

    /**
     * Test save method
     *
     * @covers ::save
     */
    public function testSave()
    {
        $entityToSave = $this->getNoteEntityMock();
        $entityManagerMock = Mockery::mock(EntityManagerInterface::class);
        $entityManagerMock->shouldReceive('merge')->with($entityToSave)->andReturn($entityToSave);
        $entityManagerMock->shouldReceive('persist')->once();
        $entityManagerMock->shouldReceive('flush')->once();
        $noteRepository = new NoteRepository($entityManagerMock, $this->getMetadata());
        $savedObject = $noteRepository->save($entityToSave);

        $this->assertInstanceOf(NoteInterface::class, $savedObject);
    }

    /**
     * Test save method for negative scenario
     *
     * @covers ::save
     * @expectedException \AppBundle\Repository\Note\Exception\NoteRepositoryException
     */
    public function testSaveForNegativeScenario()
    {
        $entityToSave = $this->getNoteEntityMock();

        $entityManagerMock = Mockery::mock(EntityManagerInterface::class);
        $entityManagerMock->shouldReceive('merge')->andThrow(ORMInvalidArgumentException::class);
        $entityManagerMock->shouldReceive('flush')->andThrow(ORMInvalidArgumentException::class);
        $entityManagerMock->shouldReceive('remove')->andThrow(OptimisticLockException::class);

        $noteRepository = new NoteRepository($entityManagerMock, $this->getMetadata());

        $noteRepository->save($entityToSave);
    }


    /**
     * Test get collection method
     *
     * @covers ::getCollection
     * @dataProvider correctDataForGetCollection
     *
     * @param bool $remembered
     * @param string $orderBy
     * @param int $limit
     * @param int $offset
     */
    public function testGetCollection($remembered, $orderBy, $limit, $offset)
    {
        $remembered = (int)$remembered;

        $queryBuilderMock = Mockery::mock(QueryBuilder::class);
        $queryBuilderMock->shouldReceive('where')->with("n.remembered = " . (int)$remembered)->once()->andReturnSelf();
        $queryBuilderMock->shouldReceive('orderBy')->with("n.{$orderBy}")->once()->andReturnSelf();
        $queryBuilderMock->shouldReceive('setFirstResult')->with($offset)->once()->andReturnSelf();
        $queryBuilderMock->shouldReceive('setMaxResults')->with($limit)->once()->andReturnSelf();
        $queryBuilderMock->shouldReceive('getQuery')->once()->andReturnSelf();
        $queryBuilderMock->shouldReceive('getResult')->once()->andReturn([]);

        $noteRepositoryMock = Mockery::mock(NoteRepository::class);
        $noteRepositoryMock->shouldReceive('createQueryBuilder')->once()->andReturn($queryBuilderMock);
        $noteRepositoryMock->makePartial();

        $list = $noteRepositoryMock->getCollection($remembered, $orderBy, $limit, $offset);

        $this->assertInternalType('array', $list);
    }

    /**
     * Test get collection method for negative scenario
     *
     * @covers ::getCollection
     * @dataProvider correctDataForGetCollection
     * @expectedException \AppBundle\Repository\Note\Exception\NoteRepositoryException
     *
     * @param bool $remembered
     * @param string $orderBy
     * @param int $limit
     * @param int $offset
     */
    public function testGetCollectionWithNegativeScenario($remembered, $orderBy, $limit, $offset)
    {
        $noteRepositoryMock = Mockery::mock(NoteRepository::class);
        $noteRepositoryMock->shouldReceive('createQueryBuilder')->andThrow(NoResultException::class);
        $noteRepositoryMock->shouldReceive('createQueryBuilder')->andThrow(NonUniqueResultException::class);
        $noteRepositoryMock->makePartial();

        $noteRepositoryMock->getCollection($remembered, $orderBy, $limit, $offset);
    }
}
