<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Note\Note;
use AppBundle\Entity\Note\NoteInterface;
use AppBundle\Form\NoteType;
use AppBundle\Service\Note\Exception\NoteServiceException;
use AppBundle\Service\Note\NoteServiceInterface;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations\QueryParam;

/**
 * Class NoteController
 * @package AppBundle\Controller
 * @Route(service="NoteController")
 */
class NoteController extends FOSRestController
{

    /**
     * Note service
     *
     * @var NoteServiceInterface
     */
    private $noteService;

    /**
     * NoteController constructor.
     * @param NoteServiceInterface $noteService
     */
    public function __construct(NoteServiceInterface $noteService)
    {
        $this->noteService = $noteService;
    }

    /**
     * Get note action
     *
     * @Rest\View
     * @Rest\Get("/notes/{id}")
     *
     * @param int $id
     * @return NoteInterface
     */
    public function getNoteAction($id)
    {
        $note = $this->noteService->getById($id);

        return $note;
    }

    /**
     * Get notes action
     *
     * @Rest\View
     * @Rest\Get("/notes")
     *
     * @QueryParam(name="page", requirements="\d+", default=1, description="Page number")
     * @QueryParam(name="limit", requirements="\d+", default=25, description="Notes number per page")
     * @QueryParam(name="remembered", requirements="\w+", default="false", description="Is note remembered")
     * @QueryParam(name="orderBy", requirements="\w+", default="createdAt", description="Order by")
     *
     * @param string $remembered
     * @param string $orderBy
     * @param int $page
     * @param int $limit
     *
     * @return NoteInterface[]|array
     */
    public function getNotesAction($remembered, string $orderBy, int $page, int $limit)
    {
        $offset = ($page - 1) * $limit;
        $remembered = filter_var($remembered, FILTER_VALIDATE_BOOLEAN);

        $total = $this->noteService->count($remembered);
        $notes = $this->noteService->getCollection($remembered, $orderBy, $limit, $offset);

        return [
            'metadata' => [
                'page' => $page,
                'limit' => $limit,
                'totalItems' => $total,
                'remembered' => $remembered,
                'totalPages' => (int)ceil($total / $limit)
            ],
            'results' => $notes
        ];
    }

    /**
     * Update action
     *
     * @Rest\Put("/notes/{id}")
     * @Rest\View(statusCode=200)
     *
     * @throws NoteServiceException
     *
     * @param Request $request
     *
     * @return View
     */
    public function updateNoteAction(Request $request)
    {
        $note = $this->noteService->getById($request->get('id'));
        $form = $this->createForm(NoteType::class, $note);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            $errors = [];
            foreach ($form->getErrors(true, true) as $formError) {
                $errors[] = $formError->getMessage();
            }

            return View::create(['errors' => $errors], 400);
        }

        try {
            $note = $this->noteService->save($note);
        } catch (NoteServiceException $e) {
            return View::create(['errors' => [$e]], 400);
        }

        return View::create($note);
    }

    /**
     * Add note action
     *
     * @Rest\Post("/notes")
     * @Rest\View(statusCode=200)
     *
     * @param Request $request
     *
     * @return View
     */
    public function addNoteAction(Request $request)
    {
        $noteEntity = new Note();
        $form = $this->createForm(NoteType::class, $noteEntity);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            $errors = [];
            foreach ($form->getErrors(true, true) as $formError) {
                $errors[] = $formError->getMessage();
            }

            return View::create(['errors' => $errors], 400);
        }

        try {
            $note = $this->noteService->save($noteEntity);
        } catch (NoteServiceException $e) {
            return View::create(['errors' => [$e]], 400);
        }

        return View::create($note);
    }
}