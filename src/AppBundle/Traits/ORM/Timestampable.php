<?php

namespace AppBundle\Traits\ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use DateTime;

/**
 * Class Timestampable
 *
 * @package AppBundle\Traits
 */
trait Timestampable
{
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * Return creation time
     *
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Return update time
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
