<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Note\Note;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use Faker\Factory;
use Faker\Generator;

/**
 * Class NoteFixtures
 * @package AppBundle\DataFixtures\ORM
 */
class NoteFixtures implements FixtureInterface
{
    /**
     * @var Generator
     */
    private $faker;

    /**
     * NoteFixtures constructor.
     */
    public function __construct()
    {
        $this->faker = (new Factory())->create();
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 50; $i++) {
            $userAdmin = new Note();
            $userAdmin->setTitle($this->faker->text(40));
            $userAdmin->setContent($this->faker->text(2000));
            $userAdmin->setRemembered($this->faker->boolean());

            $manager->persist($userAdmin);
            $manager->flush();
        }
    }
}