<?php

namespace AppBundle\Factory\Note;

use AppBundle\Entity\Note\NoteInterface;

/**
 * Class AbstractNoteFactory
 * @package AppBundle\Factory\Note
 */
abstract class AbstractNoteFactory
{
    /**
     * Create note object
     *
     * @return NoteInterface
     */
    abstract public function create(): NoteInterface;
}