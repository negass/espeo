<?php

namespace AppBundle\Service\Note\Exception;

use Exception;

/**
 * Class NoteServiceException
 * @package AppBundle\Service\Note\Exception
 */
class NoteServiceException extends Exception
{

}