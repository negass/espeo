<?php

namespace AppBundle\Service\Note;

use AppBundle\Entity\Note\NoteInterface;
use AppBundle\Service\Note\Exception\NoteServiceException;

/**
 * Interface NoteServiceInterface
 * @package AppBundle\Service\Note
 */
interface NoteServiceInterface
{
    /**
     * Default offset
     */
    const DEFAULT_OFFSET = 0;

    /**
     * Default limit
     */
    const DEFAULT_LIMIT = 25;

    /**
     * Default remember
     */
    const DEFAULT_IS_REMEMBERED = false;

    /**
     * @inheritdoc
     *
     * @throws NoteServiceException
     *
     * @param int $id
     * @return NoteInterface
     */
    public function getById(int $id): NoteInterface;

    /**
     * @inheritdoc
     *
     * @param boolean $isRemembered
     * @param string $orderBy
     * @param int $limit
     * @param int $offset
     *
     * @throws NoteServiceException
     *
     * @return NoteInterface[]|array
     */
    public function getCollection($isRemembered, $orderBy, $limit, $offset): array;

    /**
     * @inheritdoc
     *
     * @param bool $remembered
     */
    public function count($remembered = self::DEFAULT_IS_REMEMBERED): int;

    /**
     * @inheritdoc
     *
     * @param NoteInterface $note
     *
     * @throws NoteServiceException
     */
    public function save(NoteInterface $note): NoteInterface;
}