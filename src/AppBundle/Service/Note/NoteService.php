<?php

namespace AppBundle\Service\Note;

use AppBundle\Entity\Note\NoteInterface;
use AppBundle\Repository\Note\Exception\NoteRepositoryException;
use AppBundle\Repository\Note\NoteRepositoryInterface;
use AppBundle\Service\Note\Exception\NoteServiceException;

/**
 * Class NoteService
 * @package AppBundle\Service\Note
 */
class NoteService implements NoteServiceInterface
{

    /**
     * Note repository
     *
     * @var NoteRepositoryInterface
     */
    protected $noteRepository;

    /**
     * NoteService constructor.
     *
     * @param NoteRepositoryInterface $noteRepository
     */
    public function __construct(NoteRepositoryInterface $noteRepository)
    {
        $this->noteRepository = $noteRepository;
    }

    /**
     * @inheritdoc
     *
     * @param int $id
     *
     * @throws NoteServiceException
     *
     * @return NoteInterface
     */
    public function getById(int $id): NoteInterface
    {
        try {
            $note = $this->noteRepository->getById($id);
        } catch (NoteRepositoryException $e) {
            throw new NoteServiceException("Note with id {$id} does not exist", $e->getCode(), $e);
        }

        return $note;
    }

    /**
     * @inheritdoc
     *
     * @param NoteInterface $note
     *
     * @throws NoteServiceException
     *
     * @return NoteInterface
     */
    public function save(NoteInterface $note): NoteInterface
    {
        try {
            return $this->noteRepository->save($note);
        } catch (NoteRepositoryException $e) {
            throw new NoteServiceException("Error occurred while saving a note", $e->getCode(), $e);
        }
    }

    /**
     * @inheritdoc
     *
     * @param bool $remembered
     *
     * @throws NoteServiceException
     *
     * @return int
     */
    public function count($remembered = self::DEFAULT_IS_REMEMBERED): int
    {
        try {
            return $this->noteRepository->count($remembered);
        } catch (NoteRepositoryException $e) {
            throw new NoteServiceException("Error occurred while trying to count notes", $e->getCode(), $e);
        }
    }

    /**
     * @inheritdoc
     *
     * @param boolean $isRemembered
     * @param string $orderBy
     * @param int $limit
     * @param int $offset
     *
     * @throws NoteServiceException
     *
     * @return NoteInterface[]|array
     */
    public function getCollection($isRemembered, $orderBy, $limit, $offset): array
    {
        try {
            $notes = $this->noteRepository->getCollection($isRemembered, $orderBy, $limit, $offset);
        } catch (NoteRepositoryException $e) {
            throw new NoteServiceException("Error occurred while trying to fetch notes", $e->getCode(), $e);
        }

        return $notes;
    }
}