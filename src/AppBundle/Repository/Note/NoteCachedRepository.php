<?php

namespace AppBundle\Repository\Note;

use AppBundle\Entity\Note\NoteInterface;
use AppBundle\Repository\Note\Exception\NoteRepositoryException;
use AppBundle\Service\Note\Exception\NoteServiceException;
use Tedivm\StashBundle\Service\CacheService;

/**
 * Class NoteCachedRepository
 * @package AppBundle\Repository\Note
 */
class NoteCachedRepository implements NoteRepositoryInterface
{
    /**
     * Cache prefix
     */
    const CACHE_PREFIX = 'note';

    /**
     * Cache item prefix
     */
    const CACHE_ITEM_PREFIX = 'item';

    /**
     * Cache list prefix
     */
    const CACHE_LIST_PREFIX = 'list';

    /**
     * Cache count prefix
     */
    const CACHE_COUNT_PREFIX = 'count';

    /**
     * Cache service
     *
     * @var CacheService
     */
    private $cacheService;

    /**
     * Note repository
     *
     * @var NoteRepositoryInterface
     */
    private $nonCachedRepository;

    /**
     * NoteRedisRepository constructor.
     *
     * @param NoteRepositoryInterface $nonCachedRepository
     * @param CacheService $cacheService
     */
    public function __construct(NoteRepositoryInterface $nonCachedRepository, CacheService $cacheService)
    {
        $this->cacheService = $cacheService;
        $this->nonCachedRepository = $nonCachedRepository;
    }

    /**
     * @inheritdoc
     *
     * @throws NoteRepositoryException
     *
     * @param bool $remembered
     *
     * @return int
     */
    public function count(bool $remembered): int
    {
        $cacheKey = $this->getCacheKeyForCount($remembered);
        $item = $this->cacheService->getItem($cacheKey);

        if ($item->isMiss()) {
            $count = $this->nonCachedRepository->count($remembered);
            $item->set($count);
            $this->cacheService->save($item);
        }

        return $item->get();
    }

    /**
     * @inheritdoc
     *
     * @param integer $id
     *
     * @throws NoteRepositoryException
     *
     * @return NoteInterface
     */
    public function getById(int $id): NoteInterface
    {
        $cacheKey = $this->getCacheKeyForGetById($id);
        $item = $this->cacheService->getItem($cacheKey);

        if ($item->isMiss()) {
            $note = $this->nonCachedRepository->getById($id);
            $item->set($note);
            $this->cacheService->save($item);
        }

        return $item->get();
    }

    /**
     * @inheritdoc
     *
     * @param boolean $isRemembered
     * @param string $orderBy
     * @param integer $limit
     * @param integer $offset
     *
     * @return NoteInterface[]|array
     */
    public function getCollection(bool $isRemembered, string $orderBy, int $limit, int $offset): array
    {
        $cacheKey = $this->getCacheKeyForGetCollection($isRemembered, $orderBy, $limit, $offset);
        $item = $this->cacheService->getItem($cacheKey);

        if ($item->isMiss()) {
            $list = $this->nonCachedRepository->getCollection($isRemembered, $orderBy, $limit, $offset);
            $item->set($list);
            $this->cacheService->save($item);
        }

        return $item->get();
    }

    /**
     * @inheritdoc
     *
     * @param NoteInterface $note
     *
     * @throws NoteRepositoryException
     */
    public function save(NoteInterface $note): NoteInterface
    {
        $note = $this->nonCachedRepository->save($note);
        $this->invalidateCacheItem($note);
        $this->invalidateCacheList();
        $this->invalidateCacheCounter();
        $this->saveItemInCache($note);

        return $note;
    }

    /**
     * Save item in cache service
     *
     * @param NoteInterface $note
     */
    private function saveItemInCache(NoteInterface $note)
    {
        $item = $this->cacheService->getItem($this->getCacheKeyForGetById($note->getId()));
        $item->set($note);
        $this->cacheService->save($item);
    }

    /**
     * Get cache key for count method
     *
     * @param bool $remembered
     *
     * @return string
     */
    private function getCacheKeyForCount(bool $remembered): string
    {
        $remembered = (string)$remembered;

        return self::CACHE_PREFIX . ":" . self::CACHE_COUNT_PREFIX . ":remembered:{$remembered}";
    }

    /**
     * Get cache key for get by id method
     *
     * @param int $id
     * @return string
     */
    private function getCacheKeyForGetById(int $id): string
    {
        return self::CACHE_PREFIX . ":" . self::CACHE_ITEM_PREFIX . ":{$id}";
    }

    /**
     * Get cache key for get collection method
     *
     * @param bool $isRemembered
     * @param string $orderBy
     * @param int $limit
     * @param int $offset
     *
     * @return string
     */
    private function getCacheKeyForGetCollection(bool $isRemembered, string $orderBy, int $limit, int $offset): string
    {
        $isRemembered = (string)$isRemembered;

        return self::CACHE_PREFIX . ":" . self::CACHE_LIST_PREFIX .
        ":remembered:{$isRemembered}:order:{$orderBy}:limit:{$limit}:offset:{$offset}";
    }

    /**
     * Invalidate note cache
     *
     * @param NoteInterface $note
     */
    private function invalidateCacheItem(NoteInterface $note)
    {
        $cacheKey = $this->getCacheKeyForGetById($note->getId());
        $this->cacheService->deleteItem($cacheKey);
    }

    /**
     * Invalidate list cache
     */
    private function invalidateCacheList()
    {
        $this->cacheService->deleteItem(self::CACHE_PREFIX . ':' . self::CACHE_LIST_PREFIX);
    }

    /**
     * Invalidate counter cache
     */
    private function invalidateCacheCounter()
    {
        $this->cacheService->deleteItem(self::CACHE_PREFIX . ':' . self::CACHE_COUNT_PREFIX);
    }

}