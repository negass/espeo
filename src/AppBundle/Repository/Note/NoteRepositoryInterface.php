<?php

namespace AppBundle\Repository\Note;

use AppBundle\Entity\Note\NoteInterface;
use AppBundle\Repository\Note\Exception\NoteRepositoryException;

/**
 * Interface NoteRepositoryInterface
 * @package AppBundle\Repository\Note
 */
interface NoteRepositoryInterface
{

    /**
     * @inheritdoc
     *
     * @throws NoteRepositoryException
     *
     * @param bool $remembered
     *
     * @return int
     */
    public function count(bool $remembered):int;

    /**
     * @inheritdoc
     *
     * @param integer $id
     *
     * @throws NoteRepositoryException
     *
     * @return NoteInterface
     */
    public function getById(int $id):NoteInterface;

    /**
     * @inheritdoc
     *
     * @throws NoteRepositoryException
     *
     * @param boolean $isRemembered
     * @param string $orderBy
     * @param integer $limit
     * @param integer $offset
     *
     * @return NoteInterface[]|array
     */
    public function getCollection(bool $isRemembered, string $orderBy, int $limit, int $offset): array;

    /**
     * @inheritdoc
     *
     * @param NoteInterface $note
     *
     * @throws NoteRepositoryException
     */
    public function save(NoteInterface $note): NoteInterface;
}