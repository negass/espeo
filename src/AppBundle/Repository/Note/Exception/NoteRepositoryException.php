<?php

namespace AppBundle\Repository\Note\Exception;

use Exception;

/**
 * Class NoteRepositoryException
 * @package AppBundle\Repository\Note\Exception
 */
class NoteRepositoryException extends Exception
{

}