<?php

namespace AppBundle\Repository\Note;

use AppBundle\Entity\Note\NoteInterface;
use AppBundle\Repository\Note\Exception\NoteRepositoryException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;

use Exception;

/**
 * Class NoteRepository
 * @package AppBundle\Repository\Note
 */
class NoteRepository extends EntityRepository implements NoteRepositoryInterface
{
    /**
     * @inheritdoc
     *
     * @throws NoteRepositoryException
     *
     * @param integer $id
     *
     * @return NoteInterface
     */
    public function getById(int $id): NoteInterface
    {
        $note = $this->find($id);

        if ($note === null) {
            throw new NoteRepositoryException("Note with id {$id} does not exist");
        }

        return $note;
    }

    /**
     * @inheritdoc
     *
     * @throws NoteRepositoryException
     *
     * @param bool $remembered
     *
     * @return int
     */
    public function count(bool $remembered): int
    {
        try {
            $query = $this->createQueryBuilder('n')
                ->select('COUNT(n)')
                ->where("n.remembered = " . (int)$remembered)
                ->getQuery();

            return $query->getSingleScalarResult();
        } catch (ORMException $e) {
            throw new NoteRepositoryException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @inheritdoc
     *
     * @throws NoteRepositoryException
     *
     * @param boolean $isRemembered
     * @param string $orderBy
     * @param integer $limit
     * @param integer $offset
     *
     * @return NoteInterface[]|array
     */
    public function getCollection(bool $isRemembered, string $orderBy, int $limit, int $offset): array
    {
        try {
            $query = $this->createQueryBuilder('n')
                ->where("n.remembered = " . (int)$isRemembered)
                ->orderBy("n.{$orderBy}", "DESC")
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery();

            return $query->getResult();
        } catch (ORMException $e) {
            throw new NoteRepositoryException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @inheritdoc
     *
     * @param NoteInterface $note
     *
     * @throws NoteRepositoryException
     *
     * @return NoteInterface
     */
    public function save(NoteInterface $note): NoteInterface
    {
        try {
            $entity = $this->getEntityManager()->merge($note);
            $this->getEntityManager()->persist($entity);
            $this->getEntityManager()->flush();

            return $entity;
        } catch (Exception $e) {
            throw new NoteRepositoryException("Error occurred while saving a note", $e->getCode(), $e);
        }
    }
}
