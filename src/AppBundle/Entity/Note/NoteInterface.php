<?php

namespace AppBundle\Entity\Note;

/**
 * Class NoteInterface
 * @package AppBundle\Entity\Note
 */
interface NoteInterface
{
    /**
     * Get id
     *
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getContent();

    /**
     * @param string $content
     */
    public function setContent($content);

    /**
     * @return boolean
     */
    public function isRemembered();

    /**
     * @param boolean $remembered
     */
    public function setRemembered($remembered);
}