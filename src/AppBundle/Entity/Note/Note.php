<?php

namespace AppBundle\Entity\Note;

use AppBundle\Traits\ORM\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Note
 *
 * @ORM\Table(name="note")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Note\NoteRepository")
 */
class Note implements NoteInterface
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message = "Note title cannot be empty")
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @Assert\NotBlank(message = "Note content cannot be empty")
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isRemembered", type="boolean", options={"default" = false})
     */
    private $remembered;


    /**
     * @inheritdoc
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @inheritdoc
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @inheritdoc
     *
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @inheritdoc
     *
     * @return boolean
     */
    public function isRemembered()
    {
        return $this->remembered;
    }

    /**
     * @inheritdoc
     *
     * @param boolean $remembered
     */
    public function setRemembered($remembered)
    {
        $this->remembered = $remembered;
    }
}

